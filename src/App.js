import logo from "./assets/logos/logo.png";
import "./scss/components/App.css";
import { EuiFlexGroup, EuiFlexItem, EuiImage, EuiCard, EuiSpacer } from "@elastic/eui";
import LoginForm from "./components/LoginForm";

function App() {
  return (
    <EuiFlexGroup justifyContent="spaceAround">
      <EuiFlexItem grow={false}>
        <EuiFlexGroup justifyContent="spaceAround">
          <EuiFlexItem grow={false}>
            <EuiImage className="logo" size={195} alt="elastic_logo" src={logo} />
          </EuiFlexItem>
        </EuiFlexGroup>
        <EuiSpacer size="xxl" />
        <LoginForm />
      </EuiFlexItem>
    </EuiFlexGroup>
  );
}

export default App;
