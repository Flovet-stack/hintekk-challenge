import { useGeneratedHtmlId } from "@elastic/eui";
import React, { Component, useState } from "react";
import googleLogo from "../assets/logos/google.svg";
import microsoftLogo from "../assets/logos/microsoft.svg";
import validator from "validator";

import {
  EuiButton,
  EuiFieldText,
  EuiForm,
  EuiFormRow,
  EuiFieldPassword,
  EuiPanel,
  EuiIcon,
  EuiTextColor,
  EuiSpacer,
  EuiImage,
  EuiText,
  EuiFlexGroup,
  EuiFlexItem,
} from "@elastic/eui";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      dual: true,
      errors: {},
    };
  }


  handleSubmit = (e) => {
    e.preventDefault();
    console.log(this.state.errors)
    if (!this.handleValidation()) {
      return;
    }
  };

  // function to handle validation 
  handleValidation() {
    console.log(this.state.errors)
    const { email, password } = this.state;
    let errors = {};
    let formIsValid = true;

    // validate email
    if (!email) {
      formIsValid = false;
      errors["email"] = "email is required";
    } else if (!validator.isEmail(email)) {
      formIsValid = false;
      errors["email"] = "email should be a valid email";
    }

    // validate password
    if (!password) {
      formIsValid = false;
      errors["password"] = "password is required";
    }

    this.setState({ errors: errors });
    return formIsValid;
  }


  // updating state value on input change 
  handleChange = (e) => {
    const { name, value } = e.target;
    console.log(value)
    this.setState({ [name]: value });
  };

  render() {
    const { email, password } = this.state;
    return (
      <EuiPanel paddingSize="l">
        <EuiForm component="form" onSubmit={this.handleSubmit} className="login-form">
          <EuiTextColor>
            <EuiText textAlign="center">
              <h3>Log In</h3>
              <EuiSpacer size="s" />
            </EuiText>
          </EuiTextColor>
          <EuiFormRow fullWidth label="Email">
            <EuiFieldText
              fullWidth
              placeholder=""
              prepend={<EuiIcon type="user" />}
              isInvalid={this.state.errors["email"] ? true : false}
              id="email"
              name="email"
              value={email}
              onChange={this.handleChange}
            />
          </EuiFormRow>
          <EuiFormRow fullWidth label="Password">
            <EuiFieldPassword
              fullWidth
              type={this.props.dual ? "dual" : undefined}
              isInvalid={this.state.errors["password"] ? true : false}
              id="password"
              name="password"
              value={password}
              onChange={this.handleChange}
            />
          </EuiFormRow>
  
          <EuiButton type="submit" fill>
            Log In
          </EuiButton>
        </EuiForm>
        <EuiText textAlign="center">
          <EuiSpacer size="m" />
          <a href="/" className="forgot-password">
            Forgot password?
          </a>
          <EuiSpacer size="m" />
        </EuiText>
        <div className="or">
          <p>Or log in with</p>
        </div>
        <EuiFlexGroup>
          <EuiFlexItem>
            <EuiButton onClick={() => {}}>
              <EuiImage alt="elastic logo" src={googleLogo} />
              Primary
            </EuiButton>
          </EuiFlexItem>
          <EuiFlexItem>
            <EuiButton onClick={() => {}}>
              <EuiImage alt="elastic logo" src={microsoftLogo} />
              Primary
            </EuiButton>
          </EuiFlexItem>
        </EuiFlexGroup>
      </EuiPanel>
    );}

};


export default LoginForm;
